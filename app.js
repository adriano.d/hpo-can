/**
 * @author Adriano Dramisino
 * @author Matthew Grainger
 * @file
 * Main entry point for the application
 */

require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const path = require('path')
const session = require('express-session')
const flash = require('connect-flash')
const passport = require('passport')
const methodOverride = require('method-override')
const hbs = require('hbs')

// passport config
require('./config/passport')(passport)

// Set up Session
app.use(session({
    secret: 'testSecret',
    resave: true,
    saveUninitialized: true
}))

// passport middleware
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

// connect flash middleware, you can now use req.flash
app.use(flash())

// global variables
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    next()
})

// import routes
const authRoutes = require('./routes/auth-routes')
const surveyRoutes = require('./routes/survey-routes')

// connect to db
mongoose.connect('mongodb+srv://adriano:UDjrkU1OWHiWIB5e@main-p7bto.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log('connected to db!'))

// app middleware
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public'))); // need this line to access static css files
app.use('/images', express.static(__dirname + '/images'))

// redirect client to registration page when URL path is empty
app.get('/', (req, res) => { res.redirect('/user/login') })

// route middleware
app.use('/user', authRoutes)
app.use('/survey', surveyRoutes)

// configure handlebars as view engine
app.set('views', path.join(__dirname, 'views')) // where to find the views directory
app.set('view engine', 'hbs')

const PORT = process.env.PORT || 80
// listening on port 80
app.listen(PORT, () => console.log('server is running on port ' + PORT))

hbs.registerHelper("equals", question => (question == 50))
