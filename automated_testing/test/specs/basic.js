const assert = require('chai').assert

let successful_user = {
    first_name: 'Jordan',
    last_name: 'Grewal',
    phone_number: '437437437`',
    email: 'gjordan1310@gmail.com',
    company: "Jordan's company",
    size: 'Small',
    address: 'xyz street',
    postal_code: 'x1y2z3',
    country: 'Canada'
}
// Registering user for the first time
describe('Registration page', () => {   
    it('should have the right title', () => {
        browser.url('http://localhost:8080/user/register')
        const title = browser.getTitle()
        assert.strictEqual(title, '404 Survey')
        browser.pause(2000);
    })
    it('should be able to add first name', () => {
        let input = $('#registerForm > div.form-group.row > div:nth-child(1) > input')
        input.addValue('Jordan')
        value = input.getValue()
        assert.equal(value, 'Jordan')
    })
    it('should be able to add last name', () => {
        let input = $('#registerForm > div.form-group.row > div:nth-child(2) > input')
        input.addValue('Grewal')
        value = input.getValue()
        assert.equal(value, 'Grewal')
    })
    it('should be able to add Phone Number', () => {
        let input = $('#registerForm > div:nth-child(2) > input')
        input.addValue('4374374371')
        value = input.getValue()
        assert.equal(value, '4374374371')
    })
    it('should be able to add email', () => {
        let input = $('#registerForm > div:nth-child(3) > input')
        input.addValue('gjordan1310@gmail.com')
        value = input.getValue()
        assert.equal(value, 'gjordan1310@gmail.com')
    })
    it('should be able to add Company', () => {
        let input = $('#registerForm > div:nth-child(4) > input')
        input.addValue("Jordan's company")
        value = input.getValue()
        assert.equal(value, "Jordan's company")
    })
    it('should be able to add Size', () => {
        let input = $("#registerForm > div:nth-child(5) > select")
        input.selectByIndex(1);
    })
    it('should be able to add Address', () => {
        let input = $('#registerForm > div:nth-child(6) > input')
        input.addValue("xyz street")
        value = input.getValue()
        assert.equal(value, "xyz street")
    })
    it('should be able to add Postal code', () => {
        let input = $('#registerForm > div:nth-child(7) > input')
        input.addValue("x1y2z3")
        value = input.getValue()
        assert.equal(value, "x1y2z3")
    })
    it('should be able to add Country', () => {
        let input = $('#registerForm > div:nth-child(8) > input')
        input.addValue("Canada")
        value = input.getValue()
        assert.equal(value, "Canada")
    })
    it('should be able to add profit', () => {
        let input = $('#registerForm > div:nth-child(9) > input')
        input.addValue("70000")
        value = input.getValue()
        assert.equal(value, "70000")
    })
    it('should press register button successfully', () => {
        let register_button = $('#registerForm > button.btn.btn-info.submit-button').isExisting();
        assert.equal(register_button, true)
        browser.pause(3000);

        // $('#registerForm > button.btn.btn-info.submit-button').click();
        browser.pause(3000);
    })
    it('User should be registered', () => {
        let register_message = $('#registerForm > p').getHTML(false)
        browser.pause(3000);
        assert.include(register_message, "Thanks")
    })
    
})

describe("Registering same user twice", () => {

    it("should fail", () => {
        browser.url('http://localhost:8080/user/register')
        let input = $('#registerForm > div.form-group.row > div:nth-child(1) > input')
        input.addValue('Jordan')
         input = $('#registerForm > div.form-group.row > div:nth-child(2) > input')
        input.addValue('Grewal')
         input = $('#registerForm > div:nth-child(2) > input')
        input.addValue('4374374371')
         input = $('#registerForm > div:nth-child(3) > input')
        input.addValue('gjordan1310@gmail.com')
         input = $('#registerForm > div:nth-child(4) > input')
        input.addValue("Jordan's company")
        input = $("#registerForm > div:nth-child(5) > select")
        input.selectByIndex(1);
         input = $('#registerForm > div:nth-child(6) > input')
        input.addValue("xyz street")
         input = $('#registerForm > div:nth-child(7) > input')
        input.addValue("x1y2z3")
         input = $('#registerForm > div:nth-child(8) > input')
        input.addValue("Canada")
        input = $('#registerForm > div:nth-child(9) > input')
        input.addValue("70000")
        $('#registerForm > button.btn.btn-info.submit-button').click();
        let body_html = $('#registerForm > p').getHTML(false);
        browser.pause("3000");
        assert.equal('User with this email account is already registered', body_html);
 })
})
 
describe("Login to regestrition", ()=>{
    it('should have the right title', () => {
        browser.url('http://localhost:8080/user/login')
        const title = browser.getTitle()
        assert.strictEqual(title, '404 Survey')
    })
    it('Should try access code randomly and give error',()=>{
        let input = $(()=>document.getElementById('code'));
        input.addValue('hi there');
    })
    it('should press Access Survey button successfully', () => {
        let access_survey = $('#accessCodeForm > button.btn.btn-info.submit-button').isExisting();
        assert.equal(access_survey, true)
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
    })
    it("should throw error", () => {
        let register_message = $('#accessCodeForm > p').getHTML(false)
        assert.include(register_message, "Invalid Code")
    })
})