const assert = require('chai').assert

let access_codes = [  '463Wz4E4', 'HLWwi6rA', 'tm2DVYPB' ];

describe('Registration page', () => {   
    beforeEach(() => {
        browser.url('http://localhost:8080/user/login')
    })
    it('should have the right title', () => {
        const title = browser.getTitle()
        assert.strictEqual(title, 'Authenticaiton System')
    })
    it("should go to the survey page with first access code", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[0]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        browser.pause(3000);
        let title = browser.getTitle()
        assert.strictEqual(title, 'Survey')

    })
    it("should go to the survey page with second access code", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[1]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        browser.pause(3000);
        let title = browser.getTitle()
        assert.strictEqual(title, 'Survey')

    })
    it("should go to the survey page with third access code", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[2]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        browser.pause(3000);
        let title = browser.getTitle()
        assert.strictEqual(title, 'Survey')
    })
})

describe('Registering again with ', () => {   
    beforeEach(() => {
        browser.url('http://localhost:8080/user/login')
    })
    it("first access code should fail", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[0]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        let error_val = $('#accessCodeForm > p').getHTML(false);
        assert.equal(error_val, "Code has already been used")

    })
    it("second access code should fail", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[1]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        let error_val = $('#accessCodeForm > p').getHTML(false);
        assert.equal(error_val, "Code has already been used")

    })
    it("third access code should fail", ()=> {
        let input = $(()=>document.getElementById('code'));
        input.addValue(access_codes[2]);
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        let error_val = $('#accessCodeForm > p').getHTML(false);
        assert.equal(error_val, "Code has already been used")
    })
})