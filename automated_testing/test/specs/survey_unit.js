const assert = require('chai').assert

describe('Registration page', () => {

    it('should have the right title', () => {
        browser.url('http://localhost:8080/user/login')
        const title = browser.getTitle()
        assert.strictEqual(title, 'Authenticaiton System')
    })
    it("should go to the survey", () => {
        let input = $(() => document.getElementById('code'));
        let access_code = "CZt9Dqmu";   
        input.addValue(access_code);
        browser.pause("3000");
        $('#accessCodeForm > button.btn.btn-info.submit-button').click();
        let title = browser.getTitle()
        assert.strictEqual(title, 'Survey')
    })
    it("Submit first Question", () => {
        browser.pause("3000");
        let input = $("#one");
        input.click();
        input = $("#content > div > input")
        input.click();  
        browser.pause(3000);
    })
    for(let i = 0; i < 50; ++i) {
        it(i + " question submit ", question);
    }
})

function question() {
    let input = $("#one");
    input.click();
    input = $("#content > div > input")
    input.click();
}