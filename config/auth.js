/**
 * @author Adriano Dramisino
 * @file
 * Authorization middleware file
 */

/**
 * @function ensureAuthenticated
 * A middleware function that checks to see if a user has logged in 
 * before allowing them access to protected routes.
 */
module.exports = {
    ensureAuthenticated: (req, res, next) => {
        if(req.isAuthenticated()) return next()
        req.flash('error_msg', 'Enter access code to view this resource')
        res.redirect('/user/login')
    }
}