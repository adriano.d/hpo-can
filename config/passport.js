/**
 * @author Adriano Dramisino
 * @file
 * Configuration for private path authorization system is done here
 */

const LocalStrategy = require('passport-local').Strategy
const User = require('../model/User')

/**
 * @function
 * Sets up user authorization as local strategy and handles derializing, and de-serizlizing user data
 */
module.exports = passport => {
    passport.use(
        new LocalStrategy({ usernameField: 'code' }, (code, password, done) => {
            User.findOne({access_codes: code})
                .then(user => {
                    return done(null, user)
                })
                .catch(err => console.error(err))
        })
    )

    passport.serializeUser((user, done) => {
        done(null, user.id)
    })

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user)
        })
    })
}

