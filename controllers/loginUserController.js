/**
 * @author Matthew Grainer
 * @author Adriano Dramisino
 * @file
 * The controller layer for interactions between the login page and the database
 */
const User = require('../model/User')
const passport = require('passport')

/**
 * @function login_user
 * @param {Request} req
 * @param {Response} res
 * @param {Next} next
 * Handles login functionality and error/validation checking.
 */
exports.login_user = async (req, res, next) => {

  try {
    //Check to make sure input isn't empty 
    if (req.body.code == '') return res.status(400).render('login', { error: "Code Required" })
    //Check to see if code exists in db
    const user = await User.findOne({ access_codes: req.body.code })

    // check to see if code exists
    if (!user) return res.status(400).render('login', { error: "Invalid Code" })

    // check to see if code has been used, set 'used' flag to true if it hasn't and continue
    const indexToEdit = user.access_codes.indexOf(req.body.code)

    // check if code1 was used
    if(indexToEdit == 0) {
      if(user.c1_survey.used) {
        return res.status(400).render('login', { error: "Code has already been used" })
      }
      await User.updateOne({access_codes: req.body.code}, {"c1_survey.used": true}) 
    }

    // check if code2 was used
    if(indexToEdit == 1) {
      if(user.c2_survey.used) {
        return res.status(400).render('login', { error: "Code has already been used" })
      }
      await User.updateOne({access_codes: req.body.code}, {"c2_survey.used": true}) 
    }
     
    // check if code3 was used
    if(indexToEdit == 2) {
      if(user.c3_survey.used) {
        return res.status(400).render('login', { error: "Code has already been used" })
      }
      await User.updateOne({access_codes: req.body.code}, {"c3_survey.used": true}) 
    }
    
    //Send to Survey Page with Code used.
    req.session.code_used = req.body.code
    // Session Variables used for tracking survey
    // Counter tracks which question the User is on [Ex. counter = 1; Question 1]

    req.session.question_counter = 0
    req.session.dimension_counter = 0 
    req.session.answer_counter = 1
    req.session.dimension_average = 0
    req.session.save()
    
    // Send to Survey Page
    req.body.password = 'p'

    passport.authenticate('local', {
      successRedirect: '/survey',
      failureRedirect: '/user/login',
      failureFlash: true
  })(req, res, next)
    

  } catch (err) {
    res.status(400).send(err)
  }
}

/**
 * @function login_page
 * @param {Request} req
 * @param {Response} res
 * Renders the login page.
 */
exports.login_page = (req, res) => {
  // renders the view file that matches 'login.hbs'
  var code_used = req.session.code_used
  res.render('login', { code_used: code_used })
}