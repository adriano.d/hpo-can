/**
 * @author Matthew Grainger
 * @file 
 * Renders survey page
 */

/**
 * @function survey_completion_page
 * @param {Request} req
 * @param {Response} res
 * Renders the registration completion page.
 */
exports.survey_completion_page = (req, res) => {
    // renders to view file that matches 'register.hbs'
    res.render('survey_completion_page')
}