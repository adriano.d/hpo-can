/**
 * @author Adriano Dramisino
 * @author Matthew Grainger
 * @file
 * The controller layer for interactions between the login page and the database
 */

const User = require('../model/User')
const { registerValidation } = require('../utils/validation')
const keygen = require('keygenerator')
const { sendEmailWithCodes } = require('../utils/node-mailer')

/**
 * @function register_user
 * @param {Request} req
 * @param {Response} res
 * @param {Next} next 
 * Handles registration functionality and error/validation checking.
 */
exports.register_user = async (req, res, next) => {

    // validate user posted data using Joi (email is a real email, password field isn't empty, etc)
    const { error } = registerValidation(req.body)
    // if(error) return res.status(400).send(error.details[0].message)
    if (error) return res.status(400).render('register', { error: error.details[0].message, values: req.body })

    try {
        // check if email alrerady exists in db
        const emailExists = await User.findOne({ email: req.body.email })
        if (emailExists) return res.status(400).render('register', { error: 'User with this email account is already registered' })
    } catch (err) {
        res.status(400).send(err)
    }

    //Generate Codes
    const codes = [keygen.password(), keygen.password(), keygen.password()]

    // create new user
    const user = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        phone_number: req.body.phone_number,
        email: req.body.email,
        company: req.body.company,
        size: req.body.size,
        address: req.body.address,
        postal_code: req.body.postal_code,
        country: req.body.country,
        profit: req.body.profit,
        //Generate 3 keys for the account
        access_codes: [
            codes[0],
            codes[1],
            codes[2]
        ],
        c1_survey: {
            used: false,
            dimension_1: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_2: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_3: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_4: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_5: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_6: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
        },
        c2_survey: {
            used: false,
            dimension_1: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_2: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_3: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_4: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_5: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_6: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
        },
        c3_survey: {
            used: false,
            dimension_1: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_2: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_3: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_4: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_5: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0

            },
            dimension_6: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
        }
    })

    // save newly created user to db
    try {
        await user.save()
        await sendEmailWithCodes(req, codes)

        res.render("successful-registration", { user: req.body })
    } catch (err) {
        res.status(400).send(err)
    }

}

/**
 * @function register_page
 * @param {Request} req
 * @param {Response} res
 * Renders registration page.
 */
exports.register_page = (req, res) => {
    // renders to view file that matches 'register.hbs'
    res.render('register', { title: "Made it to the registration page!" })
}