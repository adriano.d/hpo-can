/**
 * @author Matthew Grainger
 * @file
 * The controller layer for interactions between the survey page and the database
 */

// Updated User Model that conatins space for 6 dimensions
const User = require('../model/User')
const survey_questions = require('../data/survey_questions')
// Used to call Python process once all surveys have been completed
const spawn = require('child_process').spawn

/**
 * @function survey_get_answer
 * @param {Request} req
 * @param {Response} res
 * @param {Next} next 
 * Hanldes majority of survey functionality
 */
exports.survey_get_answer = async (req, res, next) => {
    // Code Used at Login Page
    var code_used = req.session.code_used
    console.log("Code used at login: " + code_used)
    // Find Owner (User) of code; user = document in users collection
    const user = await User.findOne({ access_codes: code_used })
    // User's Answer to Question
    var answer = req.body.survey_answer
    // All codes owned by user account (3 total)
    var user_owned_codes = user.access_codes
    // 0 = c1_survey, 1 = c2_survey, 2 = c3_survey
    var index_to_edit = user_owned_codes.indexOf(code_used)
    var insert_statement = []
    var dimension_average_insert_statement = []
    // Creating Insert Statement for Dimension Average
    dimension_average_insert_statement.push(
        "user.",
        "c",
        (index_to_edit + 1).toString(),
        "_survey.dimension_",
        (req.session.dimension_counter + 1).toString(),
        ".average",
        "= parseFloat(req.session.dimension_average);"
    )
    // Calculate Average for Dimension
    req.session.dimension_average = parseInt(req.session.dimension_average) + parseInt(answer)
    // If reached last question in dimension reset answer counter and increment dimension counter
    if (req.session.answer_counter > 10) { 
        // Calculate Dimension Average
        req.session.dimension_average = req.session.dimension_average / 10;
        dimension_average_insert_statement = dimension_average_insert_statement.join("")
        // Persist Average
        eval(dimension_average_insert_statement)
        await user.save()
        console.log("(IN DATABASE) Before Average Calculation: " + user.c1_survey.dimension_1.average)
        console.log("(DIMENSION_AVERAGE IN PROGRAM) Before Average Calculation: " + req.session.dimension_average)
        req.session.dimension_average = 0;
        console.log("(IN DATABASE)After Average Calculation: " + user.c1_survey.dimension_1.average)
        console.log("(DIMENSION_AVERAGE IN PROGRAM)After Average Calculation: " + req.session.dimension_average)
        req.session.answer_counter = 1
        req.session.dimension_counter = req.session.dimension_counter + 1
    }
    // String Building insert statement based on position in survey and answer retrieved
    // Syntax: user.c[number]_survey.dimension_[number].answer_[number]
    // Example:  user.c1_survey.dimension_1.answer_1
    insert_statement.push(
        "user.",
        "c",
        (index_to_edit + 1).toString(),
        "_survey.dimension_",
        (req.session.dimension_counter + 1).toString(),
        ".answer_",
        (req.session.answer_counter).toString(),
        "= parseInt(answer);"
    )
    insert_statement = insert_statement.join("")
    // Final string that will be used to insert answer into database
    console.log(insert_statement)
    // insert_statement.toString() = parseInt(answer);
    eval(insert_statement)
    await user.save()
    // Incremement answer counter
    req.session.answer_counter = req.session.answer_counter + 1
    // Increment Counter to show next question on redirect
    req.session.question_counter = req.session.question_counter + 1
    // save() saves the new incremented counter to the user's session
    req.session.save()
    // Refresh survey page
    res.redirect('survey')
}

/**
 * @function survey_page
 * @param {Request} req
 * @param {Response} res 
 * Renders survey page 
 */
exports.survey_page = (req, res) => {
    // renders to view file that matches 'register.hbs'
    res.render('survey', { question: survey_questions.question_array[req.session.question_counter], 
                           layout: 'survey_layout',
                           questionCount: req.session.question_counter.toString(),
                        })
}

/**
 * @function submit_and_logout
 * @param {Request} req
 * @param {Response} res 
 * Handles submitting the survey and loggin out the current user.  
 */
exports.submit_and_logout = async (req, res) => {
    const user = await User.findOne({ access_codes: req.session.code_used })
    
    // need to store these values in varibles, wont work putting them directly into if() 
    const s1Used = await user.c1_survey.used
    const s2Used = await user.c2_survey.used
    const s3Used = await user.c3_survey.used

    // if all codes for this account have been used, call python script and begin analysis
    if(s1Used && s2Used && s3Used) {
        console.log('all codes used for this account')

        // CALL PYTHON SCRIPT HERE
        // Pass User Id once all 3 surveys have been completed
        const pythonProcess = spawn('python',['python/appMain.py', user._id])
        console.log('Python Script has been called.')
        // ***OPTIONAL CODE THAT COULD BE OF USE LATER ON ***.
        // It allows Node to receive data back from the python script.
        // pythonProcess.stdout.on('data', (data) => {
        //     //Do something with the data returned from python script
        // })
    }

    req.logout()
    // req.flash('success_msg', 'Thank you for completing the survey')
    res.render('survey_completion_page', {  success_msg: 'Thank you for completing the survey!' })
}





