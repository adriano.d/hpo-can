/**
 * @author Matthew Grainer
 * @file
 * Constants for survey questions.
 */
// Dimension 1: Digital Marketing
const d1q1 = "Our firm has a clear definition of the target customers; insights about their behavior; concise descriptions of relevant market, competitor, and customer trends; an articulation of the marketing challenge; and an explicit statement of the desired customer and business outcomes."
const d1q2 = "Our firm marketing operations increasingly rely on technology: CRM systems, big data analytics, marketing optimization tools, and a host of other specialized software."
const d1q3 = "Our firm regularly review strategic marketing investment to ensure that they are focused on the appropriate customers, well timed, effectively executed, and sensitive to evolving competitive opportunities and activities."
const d1q4 = "Our firm exploit a hybrid marketing system to achieve increased coverage, lower costs, and customized approaches – and enjoy a significant competitive advantage over rivals that cling to traditional ways;  that is, using direct sales as well as distributors, and retail sales as well as direct mail."
const d1q5 = "Our firm use knowledge-based marketing, that includes awareness of the technology in which it competes; of its competition; of its customers; of new sources of technology that can alter its competitive environment; and of its own organization, capabilities, plans, and way of doing business."
const d1q6 = "Our firm use experience-based marketing, which emphasizes interactivity, connectivity, and creativity with current and prospective customers or clients."
const d1q7 = "Our firm is on the lookout for cultural opportunity resulting from disruptions in societal norms and thus opening opportunity for innovative changes that push."
const d1q8 = "Our firm is innovating continually, using cultural flashpoints to sustain its cultural relevance by playing off particularly intriguing or contentious issues that dominate the media discourse."
const d1q9 = "Our firm use current and emerging technologies, such as high-speed communications, computer networks, and advanced software programs, companies to establish real-time dialogues with their customers and provide interactive services to  establish binding relationships with the customers or clients."
const d1q10 = "Our firm use strong digital marketing presence on social media."

// Dimension 2: Affiliate Marketing and Email Marketing
const d2q1 = "Our firm emphasizes and nurture a high performance culture."
const d2q2 = "Our firm people policies help to drive its business strategy. "
const d2q3 = "Our firm talent management practices are highly effective and the leaders committed to excellence in talent management."
const d2q4 = "Our firm actively engaged in and accountable for spotting, tracking, coaching, and developing the next generation of leaders."
const d2q5 = "Our firm talent practices are  strategically oriented, but they also put a premium on operational efficiency."
const d2q6 = "Our firm talent practices engender a strong sense of collective purpose and pride yet work very well for my career as an individual."
const d2q7 = "Our firm has a long-standing commitment to people development, and is also open to changing our policies when circumstances dictate."
const d2q8 = "Our firm staff regularly get the stretch assignments and job rotations necessarily for a more holistic development."
const d2q9 = "Our firm believe in Employment Equity and encourages diversity."
const d2q10 = "Our firm is able to deploy the right people when emerging opportunities arise, quickly and without significant disruption to other parts of your organization."

// Dimension 3: Operations Management
const d3q1 = "Our firm continually monitor and measure its business performance to spot new market opportunities, reduce costs, access new customers and increase your competitiveness."
const d3q2 = "Our firm have clearly defined key performance indicators, how to measure them and set the necessary targets to ensure continual improvements."
const d3q3 = "Our firm key performance indicators of the organization are specific, measurable, achievable, realistic and time-bound or SMART."
const d3q4 = "Our firm pursue continually improvement always deliver the best quality, and quickly response to customer demand, with maximum flexibility."
const d3q5 = "Our firm has a well-established and understood quality management standard/system. "
const d3q6 = "Our firm team up with customers to know what they buy and use, and organize product families accordingly."
const d3q7 = "Our firm adopt a unified purpose that Involve frontline employees in strategic discussions to make sure they understand the purpose of their work and have their say in what to change."
const d3q8 = "Our firm know the competition. Know their customers, their best practices, and their competitive edges."
const d3q9 = "Our firm plan and manage resources in a way the operations are close to the customer rate of use or demand."
const d3q10 = "Our firm invest in HR by offering cross-training options, job rotation, improvements in work safety and health and provide adequate rewards and recognitions."

// Dimension 4: Business Innovation
const d4q1 = "Does your organization have policies and processes like suggestion boxes, workshops or forums brainstorm and harness or ideas."
const d4q2 = "Does your organization have a supportive atmosphere in which people feel free to express their ideas without the risk of criticism or ridicule."
const d4q3 = "Does your organization encourage risk taking and experimentation - don't penalise people who try new ideas that fail."
const d4q4 = "Does your organization promote openness between individuals and teams. Good ideas and knowledge in one part of your business are regularly shared with others. Teamwork, newsletters and intranets are used to share information and encourage innovation."
const d4q5 = "Does your organization encourage a culture of share responsibility for innovation, so everybody feels involved in taking the business forward and with very few layers of management or decision making before ideas are adopted."
const d4q6 = "Does your organization reward innovation and celebrate success with appropriate incentives to encourage staff to think creatively."
const d4q7 = "Does your organization look for imagination and creativity when recruiting new employees."
const d4q8 = "Does the organization have a system in place to support and nurture the formation of cross functional teams."
const d4q9 = "Does your organisation have innovation portals or systems that define open business problems to invites solutions, screen ideas, database of past and current ideas, crowdsourcing systems, established collaborative models and the dissemination of ideas for decision or evaluation."
const d4q10 = "Does the organizations have global and local outreach networks that collaborate innovation input and outputs."

// Dimension 5: Performance Management
const d5q1 = "Does your organization encourages and nurture a high- performance culture"
const d5q2 = "Does your organization have a long-standing commitment to people development and also very open to changing policies when the circumstances dictate?"
const d5q3 = "Does your immediate superior establish plans and objectives with you?"
const d5q4 = "Do you feel that your work is satisfying and worthwhile and contributes towards the success of the organization?"
const d5q5 = "Do you have an annual set of performance standards?"
const d5q6 = "To what extend is compensation linked to performance in your organization?"
const d5q7 = "Do you get regular candid feedback on your performance?"
const d5q8 = "Does the performance management system in your organization identifies the training needs?"
const d5q9 = "Is the promotion policy in your organization linked to the performance appraisal system?"
const d5q10 = "Does your performance commensurate with a steady stream of more challenging assignments?"

const question_array = [d1q1, d1q2, d1q3, d1q4, d1q5, d1q6, d1q7, d1q8, d1q9, d1q10, 
                        d2q1, d2q2, d2q3, d2q4, d2q5, d2q6, d2q7, d2q8, d2q9, d2q10, 
                        d3q1, d3q2, d3q3, d3q4, d3q5, d3q6, d3q7, d3q8, d3q9, d3q10,
                        d4q1, d4q2, d4q3, d4q4, d4q5, d4q6, d4q7, d4q8, d4q9, d4q10, 
                        d5q1, d5q2, d5q3, d5q4, d5q5, d5q6, d5q7, d5q8, d5q9, d5q10,]
module.exports = {
    question_array: question_array
}