const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const UserSchema = Schema({
    first_name: {
        type: String,
        required: true,
        max: 255,
        min: 2
    },
    last_name: {
        type: String,
        require: true,
        max: 255,
        min: 2
    },
    phone_number: {
        type: Number,
        require: true,
        // min: 7, //Account for smaller numbers without area code.
        // max: 12 //Account for longer numbers (1-800-123-4567)
    },
    email: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    company: {
        type: String,
        required: true,
        max: 255,
        min: 2
    },
    size: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
        max: 255,
        min: 2
    },
    postal_code: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
        max: 50,
        min: 2
    },
    date: {
        type: Date,
        default: Date.now
    },
    profit: {
        type: Number,
        default: 0
    },
    access_codes: [
        {
            type: String,
            required: true,
            max: 9,
            min: 8,
        },
        {
            type: String,
            required: true,
            max: 9,
            min: 8,
        },
        {
            type: String,
            required: true,
            max: 9,
            min: 8,
        }
    ],
    c1_survey: {
        used: {
            type: Boolean,
            default: false
        },
        dimension_1: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_2: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0

        },
        dimension_3: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0

        },
        dimension_4: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_5: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        }
    },
    c2_survey: {
        used: {
            type: Boolean,
            default: false
        },
        dimension_1: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_2: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_3: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_4: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        },
        dimension_5: {
            answer_1: 0,
            answer_2: 0,
            answer_3: 0,
            answer_4: 0,
            answer_5: 0,
            answer_6: 0,
            answer_7: 0,
            answer_8: 0,
            answer_9: 0,
            answer_10: 0,
            average: 0
        }},
        c3_survey: {
            used: {
                type: Boolean,
                default: false
            },
            dimension_1: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_2: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_3: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_4: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            },
            dimension_5: {
                answer_1: 0,
                answer_2: 0,
                answer_3: 0,
                answer_4: 0,
                answer_5: 0,
                answer_6: 0,
                answer_7: 0,
                answer_8: 0,
                answer_9: 0,
                answer_10: 0,
                average: 0
            }}
        })
    


module.exports = mongoose.model('User', UserSchema)