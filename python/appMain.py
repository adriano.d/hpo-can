import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from PIL import Image
import sendEmail

#########################################################################################
# NODE TO PYTHON
#########################################################################################
# sys used pass/access arguments sent from node
# Ex. sys.argv[1] to access arg1 (argument 1)
import sys
# TO SEND DATA BACK TO NODE FROM PYTHON:
# print(dataToSendBack)
# sys.stdout.flush()


if __name__ == "__main__":
    sendEmail.send_email()
