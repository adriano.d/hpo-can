from reportlab.pdfgen import canvas
from reportlab.lib import utils
from reportlab.lib.units import cm
from reportlab.platypus import Image


def pdf_gen():

    fileName = 'python\\Reports\\Survey report.pdf'
    title = 'HPO-CAN Report'
    bar1 = 'python\\Images\\barGraph1.png'
    bar2 = 'python\\Images\\barGraph2.png'
    pie1 = 'python\\Images\\pie1.png'
    pie2 = 'python\\Images\\pie2.png'
    pie3 = 'python\\Images\\pie3.png'
    pie4 = 'python\\Images\\pie4.png'
   
    from reportlab.lib import utils

    #this method will be used to auto resize images. To be implemented in after prototype2 demo.
    #def get_image(path, width=1*cm):
    #    img = utils.ImageReader(path)
    #    iw, ih = img.getSize()
    #    aspect = ih / float(iw)
    #    return Image(path, width=width, height=(width * aspect))

    pdf = canvas.Canvas(fileName)
    pdf.setPageSize((1000,500))
    pdf.setTitle(title)
    pdf.drawImage(bar1,15,410, width=75,height=75)
    pdf.drawImage(bar2,15,320, width=75,height=75)
    pdf.drawImage(pie1,15,230, width=75,height=75)
    pdf.drawImage(pie2,15, 15, width=200,height=200)
    pdf.drawImage(pie3,250, 15, width=200,height=200)
    pdf.drawImage(pie4,460, 15, width=50,height=50)

    pdf.save()

pdf_gen()