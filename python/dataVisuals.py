import dbConnection
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pandas as pd

def bar_graph1():
    dimensions = ['D1','D2','D3','D4','D5']
    S1AVG=np.array([3.7,3,4,4,5])
    S2AVG=np.array([3.7,4.2,3.9,4.3,5])
    S3AVG=np.array([4.5,4.2,4.6,4,4.6])

    ind = [x for x, _ in enumerate(dimensions)]

    plt.bar(ind, S1AVG, width=0.8, label='S1AVG', color='gold', bottom=S2AVG+S3AVG)
    plt.bar(ind, S2AVG, width=0.8, label='S2AVG', color='silver', bottom=S3AVG)
    plt.bar(ind, S3AVG, width=0.8, label='S3AVG', color='#CD853F')

    plt.xticks(ind, dimensions)
    plt.ylabel("Scores")
    plt.xlabel("dimensions")
    plt.legend(loc="upper right")
    plt.title("Scores by survey and dimension")

    plt.savefig('python\\Images\\barGraph1.png', bbox_inches='tight')

    plt.show()

def bar_graph2():
    x = ['D1avg', 'D2avg', 'D3avg', 'D4avg', 'D5avg']
    values = [3.966667,3.8,4.166667,4.1,4.866667]
    x_pos = [i for i, _ in enumerate(x)]

    plt.bar(x_pos, values, color='green')
    plt.xlabel("Dimension Source")
    plt.ylabel("Scores")
    plt.title("Three survey's dimension Source")
    plt.xticks(x_pos, x)
    plt.savefig('python\\Images\\barGraph2.png', bbox_inches='tight')

    plt.show()

def pie_1():
    labels ='D1avg', 'D2avg', 'D3avg', 'D4avg', 'D5avg', 'lose scores'
    sizes = [4, 3.8, 4.2, 4.1,4.9,4]
    explode = (0, 0, 0, 0,0,0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90,colors=['black', 'red', 'green', 'blue', 'yellow','cyan'])
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title("See the pencentage of each dimensions in total ",fontsize=15)
    plt.savefig('python\\Images\\pie1.png', bbox_inches='tight')

    plt.show()

def pie_2():
    labels ='s1D1avg', 's1D2avg', 's1D3avg', 's1D4avg', 's1D5avg', 'lose scores in total'
    sizes = [3.7, 3, 4, 4,5,5.3]
    explode = (0, 0, 0, 0,0,0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90,colors=['pink', 'green', 'black', 'blue', 'yellow','red'])
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title("See the pencentage of of survey 1 each dimensions",fontsize=15)
    plt.savefig('python\\Images\\pie2.png', bbox_inches='tight')


    plt.show()

def pie_3():
    labels ='s2D1avg', 's2D2avg', 's2D3avg', 's2D4avg', 's2D5avg', 'lose scores in total'
    sizes = [3.7,4.2,3.9,4.3,5,3.9]
    explode = (0, 0, 0, 0,0,0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90,colors=['green', 'blue', 'black', 'pink', 'yellow','red'])
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title("See the pencentage of survey 2 each dimensions",fontsize=15)
    plt.savefig('python\\Images\\pie3.png', bbox_inches='tight')

    plt.show()

def pie_4():
    labels ='s3D1avg', 's3D2avg', 's3D3avg', 's3D4avg', 's3D5avg', 'lose scores in total'
    sizes = [4.5,4.2,4.6,4,4.6,3.1]
    explode = (0, 0, 0, 0,0,0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title("See the pencentage of survey 3 each dimensions",fontsize=15)
    plt.savefig('python\\Images\\pie4.png', bbox_inches='tight')

    plt.show()
'''
bar_graph1()
bar_graph2()
pie_1()
pie_2()
pie_3()
pie_4()
'''