/**
 * @author Adriano Dramisino
 * @author Matthew Grainer
 * @file
 * Handles client side routing for authorization based routes and directs them to external 
 * callback functions
 */
const router = require('express').Router()
const register_user_controller = require('../controllers/registerUserController')
const login_user_controller = require('../controllers/loginUserController')

// register new user route
router.post('/register', register_user_controller.register_user)

// user login route
router.post('/login', login_user_controller.login_user)

// user login page route
router.get('/login', login_user_controller.login_page)

// user register page route
router.get('/register', register_user_controller.register_page)


module.exports = router
