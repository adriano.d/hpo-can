/**
 * @author Adriano Dramisino
 * @author Matthew Grainer
 * @file
 * Handles client side routing for survey based routes and directs them to external 
 * callback functions
 */

const router = require('express').Router()
const survey_controller = require('../controllers/surveyController')
const post_survey_controller = require('../controllers/postSurveyController')
const { ensureAuthenticated }  = require('../config/auth')

// survey Page
router.get('/', ensureAuthenticated, survey_controller.survey_page)

// submit survey and logout
router.delete('/submit', survey_controller.submit_and_logout)

// survey page route
router.post('/', survey_controller.survey_get_answer)

// survey completion route
router.get('/survey_completion_page', ensureAuthenticated, post_survey_controller.survey_completion_page)

module.exports = router
