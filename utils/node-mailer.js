/**
 * @author Adriano Dramisino
 * @author Matthew Grainger
 * @file
 * Holds the function which send email to user upon registration.
 */

/**
 * @function sendEmailWithCodes
 * @param {Request} req 
 * @param {Array} codes 
 * Sends email to registered user with auto-generated access codes.
 */
const sendEmailWithCodes = (req, codes) => new Promise((resolve, reject) => {
const mailgun = require('mailgun-js')({
                      apiKey:'51a26634c6f0d0eb7731d19ed7731b54-09001d55-a447484d',
                      domain: 'sandboxfcd5c610be254304993ad18e578dac38.mailgun.org'
                    })

    mailgun.messages().send({
        from: 'HPO Canada <dramisinoadriano@gmail.com>',
        to: req.body.email,
        subject: 'Thank You For Choosing HPO CAN',
        html: `  <p style="font-family: sans-serif;">Hi there,<br><br>Thank you for registering for the HPO Canada survey.<br>Here are your access codes<br><br><strong>${codes[0]}<br>${codes[1]}<br>${codes[2]}</strong></p><br>
                 <button style="background-color: #3498db; border-radius: 5px; text-align: center; border: solid 1px #3498db; padding:10px"><a href="http://404survey.com/user/login" style="text-decoration: none; color: rgb(248, 248, 248);  font-size: 14px; font-family: sans-serif;">Start The Survey</a></button>`
    }, (error, body) => {
        if (error)  reject()
        
        resolve(body)
        console.log(body)
    })
})

module.exports.sendEmailWithCodes = sendEmailWithCodes