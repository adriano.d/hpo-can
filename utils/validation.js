/**
 * @author Adriano Dramisino
 * @author Matthew Grainger
 * @file 
 * Functions in this file handle form validation before any user information is persisted to,
 * or queried from the database.
 */

const Joi = require('@hapi/joi')

/**
 * @function registerValidation
 * @param {Object} data
 * Handles validation and verification for user registration 
 */
const registerValidation = data => {
    const schema = Joi.object ({
        first_name: Joi.string()
            .min(2)
            .required(),
        last_name: Joi.string()
            .min(2)
            .required(),
        phone_number: Joi.number()
            .min(4)
            .required(),
        email: Joi.string()
            .min(6)
            .required()
            .email(),
        company: Joi.string()
            .min(2)
            .required(),
        size: Joi.string()
            .min(2)
            .required(),
        address: Joi.string()
            .min(2)
            .required(),
        postal_code: Joi.string()
            .required(),
        country: Joi.string()
            .min(2)
            .required(),
        profit: Joi.string()
            .min(5)
            .required()
    })
    return schema.validate(data)
}

/**
 * @function loginValidation
 * @param {Object} data
 * Handles validation and verification for user login
 */
const loginValidation = data => {
    const schema = Joi.object ({
        code: Joi.string()
            .required(),
    })
    return schema.validate(data)
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation